package animalista.cuadratica.android.ecuacioncuadratica;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;


public class MainActivity extends ActionBarActivity {

    static TextView titulo;
    static Button clear;
    static Button getLast;
    static ImageView igual;
    static EditText a;
    static EditText b;
    static EditText c;
    static TextView paso1;
    static TextView paso2;
    static TextView paso3;
    static TextView paso4;
    static TextView paso1D;
    static TextView paso2D;
    static TextView paso3D;
    static TextView paso4D;
    static TextView x1;
    static TextView x2;
    static TextView x1D;
    static TextView x2D;
    static TextView x1R;
    static TextView x2R;
    static Typeface face;
    static RelativeLayout pasos;

    String aLast = "";
    String bLast = "";
    String cLast = "";
    Boolean complex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String version = Build.VERSION.RELEASE;
        if(Integer.parseInt(String.valueOf(version.charAt(0))) > 4){
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#3037bd"));
        }

        pasos = (RelativeLayout) findViewById(R.id.pasos);
        titulo = (TextView) findViewById(R.id.ecuacion);
        clear = (Button) findViewById(R.id.clear);
        getLast = (Button) findViewById(R.id.last);
        igual = (ImageView) findViewById(R.id.resultado);
        a = (EditText) findViewById(R.id.a);
        b = (EditText) findViewById(R.id.b);
        c = (EditText) findViewById(R.id.c);
        paso1 = (TextView) findViewById(R.id.paso1);
        paso2 = (TextView) findViewById(R.id.paso2);
        paso3 = (TextView) findViewById(R.id.paso3);
        paso4 = (TextView) findViewById(R.id.paso4);
        x1 = (TextView) findViewById(R.id.x1);
        x2 = (TextView) findViewById(R.id.x2);
        paso1D = (TextView) findViewById(R.id.paso1D);
        paso2D = (TextView) findViewById(R.id.paso2D);
        paso3D = (TextView) findViewById(R.id.paso3D);
        paso4D = (TextView) findViewById(R.id.paso4D);
        x1D = (TextView) findViewById(R.id.x1D);
        x2D = (TextView) findViewById(R.id.x2D);
        x1R = (TextView) findViewById(R.id.x1R);
        x2R = (TextView) findViewById(R.id.x2R);
        face = Typeface.createFromAsset(getAssets(), "fonts/LucidaSansUnicode.ttf");

        font();


        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ic_launcher_nb);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setTitle("Solver");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#4b53e7")));


        titulo.setText("AX² + BX + C = 0");


        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText a = (EditText) findViewById(R.id.a);
                EditText b = (EditText) findViewById(R.id.b);
                EditText c = (EditText) findViewById(R.id.c);
                a.setText("");
                b.setText("");
                c.setText("");
                titulo.setText("AX² + BX + C = 0");
                igual.setVisibility(View.VISIBLE);
                pasos.setVisibility(View.GONE);
                a.setEnabled(true);
                a.setFocusable(true);
                a.setClickable(true);
                a.setFocusableInTouchMode(true);
                b.setEnabled(true);
                b.setFocusable(true);
                b.setClickable(true);
                b.setFocusableInTouchMode(true);
                c.setEnabled(true);
                c.setFocusable(true);
                c.setClickable(true);
                c.setFocusableInTouchMode(true);
            }
        });


        igual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (a.getText().toString().equals("") || b.getText().toString().equals("") || c.getText().toString().equals("") ||
                        a.getText().toString().equals(".") || b.getText().toString().equals(".") || c.getText().toString().equals(".") ||
                        a.getText().toString().equals("-") || b.getText().toString().equals("-") || c.getText().toString().equals("-") ||
                        a.getText().toString().equals("-.") || b.getText().toString().equals("-.") || c.getText().toString().equals("-.") ||
                        Double.parseDouble(a.getText().toString()) == 0) {
                    AlertDialog d = new AlertDialog.Builder(MainActivity.this).create();
                    d.setMessage("\n" + "Please check the data" + "\n");
                    d.setButton("Go Back", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    d.show();
                } else {

                    formula(a.getText().toString(), b.getText().toString(), c.getText().toString());
                    if (!b.getText().toString().contains("-") && !c.getText().toString().contains("-")){
                        titulo.setText(a.getText().toString() + "X² + " + b.getText().toString() + "X + " + c.getText().toString() + " = 0");
                    }else if (b.getText().toString().contains("-") && !c.getText().toString().contains("-")){
                        titulo.setText(a.getText().toString() + "X² - " + df.format(-1*Double.parseDouble(b.getText().toString())) + "X + " + c.getText().toString() + " = 0");
                    }else if(!b.getText().toString().contains("-") && c.getText().toString().contains("-")){
                        titulo.setText(a.getText().toString() + "X² + " + b.getText().toString() + "X - " + df.format(-1*Double.parseDouble(c.getText().toString())) + " = 0");
                    }else{
                        titulo.setText(a.getText().toString() + "X² - " +df.format(-1*Double.parseDouble(b.getText().toString())) + "X - " + df.format(-1*Double.parseDouble(c.getText().toString())) + " = 0");
                    }
                    a.setEnabled(false);
                    a.setFocusable(false);
                    b.setEnabled(false);
                    b.setFocusable(false);
                    c.setEnabled(false);
                    c.setFocusable(false);

                    igual.setVisibility(View.INVISIBLE);
                    aLast = a.getText().toString();
                    bLast = b.getText().toString();
                    cLast = c.getText().toString();



                }
            }
        });


        getLast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!aLast.matches("") || !bLast.matches("") || !cLast.matches("")) {
                    a.setText(aLast);
                    b.setText(bLast);
                    c.setText(cLast);
                }
            }
        });
    }

    public void formula(String a, String b, String c) {

        pasos.setVisibility(View.VISIBLE);

        paso1.setText("X = " + paso1(a, b, c)[0]);
        paso1D.setText("        "+paso1(a, b, c)[1]);
        paso2.setText("X = " + paso2(a, b, c)[0]);
        paso2D.setText("        "+paso2(a, b, c)[1]);
        paso3.setText("X = " + paso3(a, b, c)[0]);
        paso3D.setText("        "+paso3(a, b, c)[1]);
        paso4.setText("X = " + paso4(a, b, c, null)[0]);
        paso4D.setText("        "+paso4(a, b, c, null)[1]);
        if (!complex) {
            x1.setText("X₁ = " + x1(a, b, c)[0]);
            x2.setText("X₂ = " + x2(a, b, c)[0]);
            x1D.setText("         " + x1(a, b, c)[1]);
            x2D.setText("         " + x2(a, b, c)[1]);
            x1R.setText("= "+ df.format(Double.parseDouble(x1(a, b, c)[0]) / (2 * Double.parseDouble(a))));
            x2R.setText("= "+ df.format(Double.parseDouble(x2(a, b, c)[0]) / (2 * Double.parseDouble(a))));
        }else{
            x1.setText("X₁ = " + x1(a, b, c)[0]);
            x2.setText("X₂ = " + x2(a, b, c)[0]);
            x1D.setText("         " + x1(a, b, c)[1]);
            x2D.setText("         " + x2(a, b, c)[1]);
            x1R.setText("");
            x2R.setText("");
        }

    }



    ////////////
    static DecimalFormat df = new DecimalFormat("#.###");
    static char por = '·';
    static char cuadrado = '²';
    static char pOm = '±';
    static char top = '̅';
    static char symRaiz = '√';
    ////////////

    //paso1
    public String[] paso1(String a, String b, String c){
        String[] nd = new String[2];
        String num = "-(" + b + ")" + pOm + raiz1(a, b, c);
        String den = "2" + por + "(" + a + ")";

        if (num.length() > den.length()){
            int space = (num.length() - den.length())/2;
            for (int i = 0; i < space-5; i++){
                den = " " + den + " ";
            }

        }else if (num.length() < den.length()) {
            int space = (den.length() - num.length()) / 2;
            for (int i = 3; i < space-5; i++) {
                num = " " + num + " ";
            }
        }
        nd[0] = num;
        nd[1]=denominador1(den);

        return nd;
    }
    public String raiz1(String a, String b, String c){
        String sqrt = "(" + b + ")" + cuadrado + " - 4" + por + "(" + a + ")" + por + "(" + c + ")";
        StringBuilder sb1 = new StringBuilder().append(symRaiz);
        for (int i = 0; i < sqrt.length(); i++){
            char chars = sqrt.charAt(i);
            sb1.append(chars).append(top);
        }
        String root = sb1.toString();
        return root;
    }
    public String denominador1(String den){
        StringBuilder sb1 = new StringBuilder();
        for (int i = 0; i < den.length(); i++){
            char chars = den.charAt(i);
            sb1.append(chars).append(top);
        }
        String denp = sb1.toString();
        return denp;
    }

    //paso2
    public String[] paso2(String a, String b, String c){
        String[] nd = new String[2];
        String num = String.valueOf(df.format(-1*Double.parseDouble(b))) + pOm + raiz2(a, b, c);
        String den = String.valueOf(df.format(2 * Double.parseDouble(a)));

        if (num.length() > den.length()){
            int space = (num.length() - den.length())/2;
            for (int i = 1; i < space; i++){
                den = " " + den + " ";
            }

        }else if (num.length() < den.length()) {
            int space = (den.length() - num.length()) / 2;
            for (int i = 1; i < space; i++) {
                num = " " + num + " ";
            }
        }
        nd[0] = num;
        nd[1]=denominador2(den);

        return nd;
    }
    public String raiz2(String a, String b, String c){
        double m4ac = 4*Double.parseDouble(a)*Double.parseDouble(c);
        String sqrt;
        if (m4ac >=0){
            sqrt = df.format(Math.pow(Double.parseDouble(b),2)) + " - " + String.valueOf(df.format(m4ac));
        }else {
            sqrt = df.format(Math.pow(Double.parseDouble(b),2)) + " - (" + String.valueOf(df.format(m4ac) + ")");
        }

        StringBuilder sb1 = new StringBuilder().append(symRaiz);
        for (int i = 0; i < sqrt.length(); i++){
            char chars = sqrt.charAt(i);
            sb1.append(chars).append(top);
        }
        String root = sb1.toString();
        return root;
    }
    public String denominador2(String den){
        StringBuilder sb1 = new StringBuilder();
        for (int i = 0; i < den.length(); i++){
            char chars = den.charAt(i);
            sb1.append(chars).append(top);
        }
        String denp = sb1.toString();
        return denp;
    }

    //paso3
    public String[] paso3(String a, String b, String c){
        String[] nd = new String[2];
        String num = String.valueOf(df.format(-1*Double.parseDouble(b))) + pOm + raiz3(a, b, c);
        String den = String.valueOf(df.format(2*Double.parseDouble(a)));

        if (num.length() > den.length()){
            int space = (num.length() - den.length())/2;
            for (int i = 0; i < space; i++){
                den = " " + den + " ";
            }

        }else if (num.length() < den.length()) {
            int space = (den.length() - num.length()) / 2;
            for (int i = 0; i < space; i++) {
                num = " " + num + " ";
            }
        }
        nd[0] = num;
        nd[1]=denominador3(den);

        return nd;
    }
    public String raiz3(String a, String b, String c){
        String sqrt = String.valueOf(df.format(Math.pow(Double.parseDouble(b), 2) - 4 * Double.parseDouble(a) * Double.parseDouble(c)));
        StringBuilder sb1 = new StringBuilder().append(symRaiz);
        for (int i = 0; i < sqrt.length(); i++){
            char chars = sqrt.charAt(i);
            sb1.append(chars).append(top);
        }
        String root = sb1.toString();
        return root;
    }
    public String denominador3(String den){
        StringBuilder sb1 = new StringBuilder();
        for (int i = 0; i < den.length(); i++){
            char chars = den.charAt(i);
            sb1.append(chars).append(top);
        }
        String denp = sb1.toString();
        return denp;
    }

    //paso4
    public String[] paso4(String a, String b, String c, String mm){
        String[] nd = new String[2];
        String num;
        if (mm == null){
            num = String.valueOf(df.format(-1*Double.parseDouble(b))) + pOm + raiz4(a, b, c);
        }else{
            if(mm.matches("plus")){
                num = String.valueOf(df.format(-1*Double.parseDouble(b))) + "+" + raiz4(a, b, c);
            }else{
                num = String.valueOf(df.format(-1*Double.parseDouble(b))) + "-" + raiz4(a, b, c);
            }
        }

        String den = String.valueOf(df.format(2 * Double.parseDouble(a)));

        if (num.length() > den.length()){
            int space = (num.length() - den.length())/2;
            for (int i = 0; i < space+2; i++){
                den = " " + den + " ";
            }

        }else if (num.length() < den.length()) {
            int space = (den.length() - num.length()) / 2;
            for (int i = 0; i < space+2; i++) {
                num = " " + num + " ";
            }
        }
        nd[0] = num;
        nd[1]=denominador4(den);

        return nd;
    }
    public String raiz4(String a, String b, String c){
        if (Math.pow(Double.parseDouble(b),2) - 4*Double.parseDouble(a)*Double.parseDouble(c) >= 0){
            complex = false;
            System.out.println("Raiz4, complex: " + complex);
            return String.valueOf(df.format(Math.sqrt(Math.pow(Double.parseDouble(b), 2) - 4 * Double.parseDouble(a) * Double.parseDouble(c))));
        }else {
            complex = true;
            System.out.println("Raiz4, complex: " + complex);
            return String.valueOf(df.format(Math.sqrt(-1*(Math.pow(Double.parseDouble(b),2) - 4*Double.parseDouble(a)*Double.parseDouble(c))))) + "i";
        }
    }
    public String denominador4(String den){
        StringBuilder sb1 = new StringBuilder();
        for (int i = 0; i < den.length(); i++){
            char chars = den.charAt(i);
            sb1.append(chars).append(top);
        }
        String denp = sb1.toString();
        return denp;
    }

    //x1
    public String[] x1(String a, String b, String c) {
        String[] nd = new String[2];
        if (raizx1(a, b, c).contains("i")) {
            return paso4(a,b,c, "plus");
        } else {
            String num = String.valueOf(df.format(-Double.parseDouble(b) + Double.parseDouble(raizx1(a, b, c))));
            String den = String.valueOf(df.format(2 * Double.parseDouble(a)));

            if (num.length() > den.length()) {
                int space = (num.length() - den.length()) / 2;
                for (int i = 0; i < space; i++) {
                    den = " " + den + " ";
                }

            } else if (num.length() < den.length()) {
                int space = (den.length() - num.length()) / 2;
                for (int i = 0; i < space; i++) {
                    num = " " + num + " ";
                }
            }
            nd[0] = num;
            nd[1] = denx1(den);

            return nd;
        }
    }
    public String raizx1(String a, String b, String c){
        if (Math.pow(Double.parseDouble(b),2) - 4*Double.parseDouble(a)*Double.parseDouble(c) >= 0){
            return String.valueOf(df.format(Math.sqrt(Math.pow(Double.parseDouble(b), 2) - 4 * Double.parseDouble(a) * Double.parseDouble(c))));
        }else {
            return String.valueOf(df.format(Math.sqrt(-1*(Math.pow(Double.parseDouble(b),2) - 4*Double.parseDouble(a)*Double.parseDouble(c))))) + "i";
        }
    }
    public String denx1(String den){
        StringBuilder sb1 = new StringBuilder();
        for (int i = 0; i < den.length(); i++){
            char chars = den.charAt(i);
            sb1.append(chars).append(top);
        }
        String denp = sb1.toString();
        return denp;
    }

    //x2
    public String[] x2(String a, String b, String c) {
        String[] nd = new String[2];
        if (raizx1(a, b, c).contains("i")) {
            return paso4(a, b, c, "minus");
        } else {
            String num = String.valueOf(df.format(-Double.parseDouble(b) - Double.parseDouble(raizx2(a, b, c))));
            String den = String.valueOf(df.format(2 * Double.parseDouble(a)));

            if (num.length() > den.length()) {
                int space = (num.length() - den.length()) / 2;
                for (int i = 0; i < space; i++) {
                    den = " " + den + " ";
                }

            } else if (num.length() < den.length()) {
                int space = (den.length() - num.length()) / 2;
                for (int i = 0; i < space; i++) {
                    num = " " + num + " ";
                }
            }
            nd[0] = num;
            nd[1] = denx2(den);

            return nd;
        }
    }
    public String raizx2(String a, String b, String c){
        if (Math.pow(Double.parseDouble(b),2) - 4*Double.parseDouble(a)*Double.parseDouble(c) >= 0){
            return String.valueOf(df.format(Math.sqrt(Math.pow(Double.parseDouble(b), 2) - 4 * Double.parseDouble(a) * Double.parseDouble(c))));
        }else {
            return String.valueOf(df.format(Math.sqrt(-1*(Math.pow(Double.parseDouble(b),2) - 4*Double.parseDouble(a)*Double.parseDouble(c))))) + "i";
        }
    }
    public String denx2(String den){
        StringBuilder sb1 = new StringBuilder();
        for (int i = 0; i < den.length(); i++){
            char chars = den.charAt(i);
            sb1.append(chars).append(top);
        }
        String denp = sb1.toString();
        return denp;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            AlertDialog about = new AlertDialog.Builder(MainActivity.this).create();
            // Res.setTitle("Result");

            about.setMessage("\nDeveloped by: Animalista\n\nContact: canalanimalista@gmail.com\n");
            about.setButton("Thanks!", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            about.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void font(){
        paso1.setTypeface(face);
        paso1.setLineSpacing(-7f, 1f);
        paso2.setTypeface(face);
        paso2.setLineSpacing(-7f, 1f);
        paso3.setTypeface(face);
        paso3.setLineSpacing(-7f, 1f);
        paso4.setTypeface(face);
        paso4.setLineSpacing(-7f, 1f);
        paso1D.setTypeface(face);
        paso1D.setLineSpacing(-7f, 1f);
        paso2D.setTypeface(face);
        paso2D.setLineSpacing(-7f, 1f);
        paso3D.setTypeface(face);
        paso3D.setLineSpacing(-7f, 1f);
        paso4D.setTypeface(face);
        paso4D.setLineSpacing(-7f, 1f);
        x1.setTypeface(face);
        x1.setLineSpacing(-7f, 1f);
        x2.setTypeface(face);
        x2.setLineSpacing(-7f, 1f);
        x1D.setTypeface(face);
        x1D.setLineSpacing(-7f, 1f);
        x2D.setTypeface(face);
        x2D.setLineSpacing(-7f, 1f);
        x1D.setTypeface(face);
        x1D.setLineSpacing(-7f, 1f);
        x1R.setTypeface(face);
        x1R.setLineSpacing(-7f, 1f);
        x2R.setTypeface(face);
        x2R.setLineSpacing(-7f, 1f);
    }
}
